/**
 * 
 */
package model.data_structures;

/**
 * @author SAMSUNG
 *
 */
public class NodoDoble<T> {
	
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	private T elemento;
	private int posicion;
	
	public NodoDoble(T pElemento,int posicion){
		elemento=pElemento;
		siguiente=null;
		anterior=null;
	}
	public T darElemento(){
		return elemento;
	}
	
	public int darPosicion(){
		return posicion;
	}
	
	public NodoDoble<T> darSiguiente(){
		return siguiente;
	}
	
	public NodoDoble<T> darAnterior(){
		return anterior;
	}
	
	public void modificarSiguiente(NodoDoble<T> nodo, int pPosicion){
		siguiente=nodo;
		posicion= pPosicion;
	}
	
	public void modificarAnterior(NodoDoble<T> nodo, int pPosicion){
		anterior=nodo;
		posicion=pPosicion;
	}
	
	public void agregarElemento(T pElemento){
		elemento= pElemento;
	}


}
