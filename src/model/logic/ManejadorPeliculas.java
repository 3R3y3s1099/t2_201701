package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ListaDobleEncadenada<VOPelicula> misPeliculas;

	private ListaDobleEncadenada<VOAgnoPelicula> peliculasAgno;



	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		FileReader lector;
		try {	

			int numeroPeli;
			String nombrePeli;
			int agnoCreacion=0;
			lector = new FileReader(archivoPeliculas);	
			BufferedReader input = new BufferedReader(lector);
			misPeliculas = new ListaDobleEncadenada<>();
			peliculasAgno= new ListaDobleEncadenada<>();

			try {
				String data= input.readLine();
				data=input.readLine();
				System.out.println("*****************inicio***********");
				while (data!=null) {
				
					ILista<String> generos = new ListaDobleEncadenada<>();
					String mitad= data.substring(data.indexOf(',')+1, data.lastIndexOf(','));
					String ultimo = data.substring(data.lastIndexOf(',')+1, data.length());
					int primero = Integer.parseInt(data.substring(0,data.indexOf(',')));

					int posI=mitad.lastIndexOf(40);
					int posF=mitad.lastIndexOf(41);

					if (posF>=0 && posI>=0 && !(mitad.substring(posI+1, posF)).equals("2007-") && (mitad.substring(posI+1, posF)).length()<5  ){
						agnoCreacion=Integer.parseInt(mitad.substring(posI+1, posF));
					}
					else{
						agnoCreacion=0;
					}


				
					generos.agregarElementoFinal(ultimo);
					VOPelicula obj =new VOPelicula();
					obj.setAgnoPublicacion(agnoCreacion);
//					if (mitad.lastIndexOf('(')>0){
//					obj.setTitulo(mitad.substring(0,mitad.lastIndexOf('(')));
//					}
//					else{
//						obj.setTitulo(mitad);
//					}
					obj.setTitulo(mitad);
					obj.setGenerosAsociados(generos);
					misPeliculas.agregarElementoFinal(obj);
                    
//			System.out.println(mitad);
					int contador =1950;
					VOAgnoPelicula obj2= new VOAgnoPelicula();
					obj2.setAgno(contador);
					peliculasAgno.agregarElementoFinal(obj2);
					contador++;

					data=input.readLine();
				}
				
				System.out.println("*****************fin***********");
				cargarAyuda();

			} catch (IOException e) {
				// TODO Auto-generated catch block

				System.out.println("ERROR READ LINE");
			}


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			System.out.println("ERROR FILE READER");
		}

	}

	public void cargarAyuda (){

		ListaDobleEncadenada<VOPelicula> copia= new ListaDobleEncadenada<>();

		for (int i=1950; i<2017 ; i++){
			VOAgnoPelicula peliAgno = new VOAgnoPelicula();
			peliAgno.setAgno(i);
			peliculasAgno.agregarElementoFinal(peliAgno);
		}
		
		peliculasAgno.moverActualInicio();
		for (int i=0; i<peliculasAgno.darNumeroElementos(); i++){
		VOAgnoPelicula actual1=peliculasAgno.darElementoPosicionActual();
		misPeliculas.moverActualInicio();
			for (int j=0; j<misPeliculas.darNumeroElementos() ; j++){
				VOPelicula actual= misPeliculas.darElementoPosicionActual();
				if (actual.getAgnoPublicacion()==actual1.getAgno()){
					copia.agregarElementoFinal(actual);
				}
				misPeliculas.avanzarSiguientePosicion();
			}
			
			actual1.setPeliculas(copia);
			copia=new ListaDobleEncadenada<>();
			peliculasAgno.avanzarSiguientePosicion();
		
		}
		
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> lista= new ListaDobleEncadenada<>();
		misPeliculas.moverActualInicio();
		for (int i=0; i<misPeliculas.darNumeroElementos();i++){
			VOPelicula actual= misPeliculas.darElementoPosicionActual();
			
			if (actual.getTitulo().contains(busqueda)){
				lista.agregarElementoFinal(actual);
			}
           misPeliculas.avanzarSiguientePosicion();
		}
//		while (misPeliculas.iterator().hasNext()) {
//			VOPelicula actual = misPeliculas.iterator().next();
//			if (actual.getTitulo().contains(busqueda)){
//				lista.agregarElementoFinal(actual);
//			}
//		}
		return lista;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {


		// TODO Auto-generated method stub
		
		ILista<VOPelicula> lista= new ListaDobleEncadenada<>();
		misPeliculas.moverActualInicio();
		for (int i=0; i<misPeliculas.darNumeroElementos();i++){
			VOPelicula actual= misPeliculas.darElementoPosicionActual();
			
			if (actual.getAgnoPublicacion()==agno){
				lista.agregarElementoFinal(actual);
			}
           misPeliculas.avanzarSiguientePosicion();
		}
		return lista;

	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
//		peliculasAgno.moverActualInicio();
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();
		
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
//        peliculasAgno.moverActualInicio();
        peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
		
		
	}

}
