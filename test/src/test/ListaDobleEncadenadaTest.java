package test;

import model.data_structures.ListaDobleEncadenada;
import junit.framework.TestCase;

public class ListaDobleEncadenadaTest extends TestCase
{
	private ListaDobleEncadenada<String> doble;
	
	private void setupEscenario1()
	{
		doble = new ListaDobleEncadenada<>();
		
		doble.agregarElementoFinal("pelicula1");
		doble.agregarElementoFinal("pelicula2");
		doble.agregarElementoFinal("pelicula3");
		doble.agregarElementoFinal("pelicula4");
	}
	
	public void testAgregarElementoFinal()
	{
		setupEscenario1();
		doble.agregarElementoFinal("pelicula5");
		assertEquals(5, doble.darNumeroElementos());
	}
	public  void testAvanzarSiguientePosicion()
	{
		setupEscenario1();
		assertEquals(true, doble.avanzarSiguientePosicion());
	}
	
	
	public void testMoverIzquierda()
	{
		setupEscenario1();
		doble.moverIzquierda();
		assertEquals("pelicula1", doble.darElementoPosicionActual());
	}
	public void testDarElemento()
	{
		setupEscenario1();
		doble.darElemento(1);
		assertEquals("pelicula1", doble.darElementoPosicionActual());
	}
	
	public void testRetrocederPosicionAnterior()
	{
		setupEscenario1();
		doble.retrocederPosicionAnterior();
		assertEquals("pelicula1", doble.darElementoPosicionActual());
	}

}
