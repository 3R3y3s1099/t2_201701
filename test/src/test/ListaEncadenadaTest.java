package test;
import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase 
{
	// Atributos
	private ListaEncadenada<String> sencilla;

	// Escenarios
	private void setupEscenario1()
	{
		sencilla = new ListaEncadenada<String>();
		sencilla.agregarElementoFinal("pelicula1");
		sencilla.agregarElementoFinal("pelicula2");
		sencilla.agregarElementoFinal("pelicula3");
		sencilla.agregarElementoFinal("pelicula4");
	}

	public void testAgregarElementoAlFinal()
	{
		setupEscenario1();
		sencilla.agregarElementoFinal("pelicula4");
		assertEquals(5, sencilla.darNumeroElementos());
	}
	public void testAvanzarSiguientePosicion()
	{
		setupEscenario1();
		sencilla.avanzarSiguientePosicion();
		assertEquals(false, sencilla.avanzarSiguientePosicion());
	}
	public void testRetrocederPosicion()
	{
		setupEscenario1();
		sencilla.avanzarSiguientePosicion();
		sencilla.retrocederPosicionAnterior();

		assertEquals(false, sencilla.retrocederPosicionAnterior());
	}
	public void testDarElemento()
	{
		setupEscenario1();
		sencilla.darElemento(4);
		assertEquals("pelicula4", sencilla.darElementoPosicionActual());

	}
}
